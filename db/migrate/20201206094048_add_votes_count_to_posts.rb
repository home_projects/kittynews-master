# frozen_string_literal: true

class AddVotesCountToPosts < ActiveRecord::Migration[6.0]
  def change
    add_column :posts, :votes_count, :integer, null: false, default: 0
  end
end
