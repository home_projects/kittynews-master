# frozen_string_literal: true

class CreatePostVotes < ActiveRecord::Migration[6.0]
  def change
    create_table :post_votes do |t|
      t.references :post, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end

    add_index :post_votes, %i[post_id user_id], unique: true
  end
end
