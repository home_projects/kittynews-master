# frozen_string_literal: true

require 'rails_helper'

describe Mutations::PostVoteManage do
  let(:object) { :not_used }
  let(:user) { create :user }
  let(:post) { create :post }

  def call(current_user:, context: {}, **args)
    context = Utils::Context.new(
      query: OpenStruct.new(schema: KittynewsSchema),
      values: context.merge(current_user: current_user),
      object: nil
    )
    described_class.new(object: nil, context: context, field: nil).resolve(args)
  end

  describe '.resolve' do
    context 'valid' do
      subject { call(current_user: user, post_id: post.id) }

      context 'user has not voted for this post yet' do
        it 'creates the vote' do
          expect { subject }.to change { PostVote.count }.by(1)
        end

        it 'returns expected result' do
          expect(subject[:votes_count]).to eq post.reload.votes_count
          expect(subject[:errors]).to eq []
        end
      end

      context 'user already has voted for this post' do
        let!(:post_vote) { create :post_vote, post: post, user: user }

        it 'destroys the existing vote' do
          expect { subject }.to change { PostVote.count }.by(-1)
        end

        it 'returns the expected result' do
          expect(subject[:votes_count]).to eq post.reload.votes_count
          expect(subject[:errors]).to eq []
        end
      end
    end

    context 'invalid' do
      it 'requires post id' do
        expect { call(current_user: user) }.to raise_error(ArgumentError)
      end

      it 'requires a logged in user' do
        expect { call(current_user: nil, post_id: post.id) }.to raise_error GraphQL::ExecutionError, 'current user is missing'
      end

      context 'validation errors in response' do
        before do
          # stub invalid instance of PostVote in initialized
          allow(PostVote).to receive(:new).and_return(PostVote.new)
        end

        it 'returns errors' do
          result = call(current_user: user, post_id: post.id)
          expect(result[:votes_count]).to eq nil
          expect(result[:errors]).to include 'Post must exist'
          expect(result[:errors]).to include 'User must exist'
        end
      end
    end
  end
end
