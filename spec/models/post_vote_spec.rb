# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PostVote, type: :model do
  let(:post) { create :post}
  let(:user) { create :user}

  context 'vote uniqueness' do
    before { create :post_vote, post: post, user: user }
    let(:duplicated_instance) { described_class.new(post: post, user: user) }

    it 'doesn\'t validate uniqueness on a model level' do
      expect { duplicated_instance.validate! }.to_not raise_error
    end

    it 'validates uniqueness on a DB level' do
      expect { duplicated_instance.save! }.to raise_error(ActiveRecord::RecordNotUnique)
    end
  end
end
