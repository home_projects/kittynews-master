# frozen_string_literal: true

FactoryBot.define do
  factory :post_vote do
    association :post
    association :user
  end
end
