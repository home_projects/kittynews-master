# frozen_string_literal: true

FactoryBot.define do
  factory :post do
    sequence(:title) { |n| "Awesome Idea ##{n}" }
    sequence(:tagline) { |n| "The awesome idea ##{n}" }
    sequence(:url) { |n| "https://example.com/#{n}" }
    association :user

    trait :with_comments do
      comments { build_list :comment, 3 }
    end
  end
end
