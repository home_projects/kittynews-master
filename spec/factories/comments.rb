# frozen_string_literal: true

FactoryBot.define do
  factory :comment do
    text { 'very special comment' }
    association :user
  end
end
