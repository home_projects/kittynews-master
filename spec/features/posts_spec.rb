# frozen_string_literal: true

require 'rails_helper'

feature 'Posts' do
  scenario 'Displaying the posts on the homepage', js: true do
    post1 = create :post
    post2 = create :post

    visit root_path

    expect(page).to have_content post1.title
    expect(page).to have_content post2.title
  end

  scenario 'Displaying the post detail page', js: true do
    post = create :post, :with_comments

    visit root_path
    click_on post.title

    expect(page).to have_content 'Comments'
    expect(page).to have_content post.title
    expect(page).to have_content post.tagline
  end

  scenario 'Displaying the post comments', js: true do
    post = create :post, :with_comments

    visit root_path
    click_on post.title

    post.comments.each do |comment|
      expect(page).to have_content comment.text
    end
  end
end
