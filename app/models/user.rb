# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                 :bigint           not null, primary key
#  email              :string           not null
#  encrypted_password :string           not null
#  name               :string           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_users_on_email  (email) UNIQUE
#
class User < ApplicationRecord
  devise :database_authenticatable, :registerable, :validatable

  has_many :user_associations, class_name: 'UserAssociation', foreign_key: :followed_by_user_id, dependent: :destroy
  has_many :followees, through: :user_associations, source: :following_user

  has_many :posts, dependent: :destroy, inverse_of: :user
  has_many :comments, dependent: :destroy, inverse_of: :user

  # NOTE: (ssavitsky)
  # We likely don't need to retrieve votes of the User
  # Regarding "has_many :post_votes, dependent: :destroy/:nullify" strategy:
  # lets say we dont destroy Users (only maybe softly)
  # now we have foreign_key constraint on DB level so a User can't be destroyed if such a user has the PostVote
  # otherwise (if we can destroy users and there are no DB constraints):
  # I would use :nullify strategy to keep users votes for existing posts even if the user was deleted

  validates :name, presence: true
end
