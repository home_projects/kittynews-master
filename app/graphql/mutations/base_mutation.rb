# frozen_string_literal: true

module Mutations
  class BaseMutation < GraphQL::Schema::Mutation
    def require_current_user!
      return if context[:current_user].present?

      raise GraphQL::ExecutionError.new('current user is missing', extensions: { code: 'AUTHENTICATION_ERROR' })
    end
  end
end
