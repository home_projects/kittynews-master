# frozen_string_literal: true

module Mutations
  class PostVoteManage < Mutations::BaseMutation
    null true

    argument :post_id, ID, required: true

    field :votes_count, Integer, null: true
    field :errors, [String], null: false

    def resolve(post_id:)
      require_current_user!

      @post = Post.find(post_id)
      @user = context[:current_user]
      @errors = []

      manage_post_vote
      respond_with_format
    end

    private

    def manage_post_vote
      vote = PostVote.find_by(post: @post, user: @user)
      if vote
        destroy_vote!(vote)
      else
        create_vote
      end
    end

    def create_vote
      post_vote = PostVote.new(post: @post, user: @user)
      post_vote.save
      @errors = post_vote.errors.full_messages if post_vote.errors.any?
    end

    def destroy_vote!(vote)
      vote.destroy!
    end

    def respond_with_format
      {
        votes_count: @errors.any? ? nil : @post.reload.votes_count,
        errors: @errors
      }
    end
  end
end
