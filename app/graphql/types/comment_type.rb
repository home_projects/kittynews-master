# frozen_string_literal: true

module Types
  class CommentType < Types::BaseObject
    field :id, ID, null: false
    field :text, String, null: false
    field :created_at, GraphQL::Types::ISO8601Date, null: false
    field :user, UserType, null: false
  end
end
