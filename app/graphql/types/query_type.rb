# frozen_string_literal: true

# NOTE(ssavitsky):
# To solve N+1 problem I used lazy_loading provided by a gem as a fast solution that works out of the box for simple scenarios
# It loads relations on demand, basically works good
# However it will not cover more complex cases, so it needs to think more how to handle it in a good way

module Types
  class QueryType < Types::BaseObject
    field :posts_all, [PostType], null: false

    field :viewer, ViewerType, null: true

    field :post, PostType, null: false do
      argument :id, ID, required: true
    end

    def posts_all
      Post.lazy_preload(:user, [comments: :user]).reverse_chronological.all
    end

    def post(id:)
      Post.lazy_preload(:user, [comments: :user]).find(id)
    end

    def viewer
      context.current_user
    end
  end
end
