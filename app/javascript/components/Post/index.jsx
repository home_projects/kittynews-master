import * as React from 'react';
import {useMutation} from "react-apollo";
import gql from "graphql-tag";
import {handleError} from "../utils/errors-handling";
import Error from "../Error";

const VOTE_QUERY = gql`
  mutation PostVoteManage($postId: ID!) {
    postVoteManage(postId: $postId) {
      votesCount
      errors
    }
  }
`;

function Post({ post }) {
  const [postVoteManage, { data, error }] = useMutation(VOTE_QUERY);

  // Request basic error handling (includes redirect if NOT_AUTHENTICATED)
  if (error) {
    const res = handleError(error);
    if (res.action) res.action();
    if (res.message) return res.message;
  }

  // Component error handling (e.g. validation errors, comes in the response, not as a request error above)
  const componentErrors = data?.postVoteManage?.errors;

  // NOTE(ssavitsky):
  // we also could use useState before to set the initial value that came in props
  // and then we could set the new State here if the data exists
  // but this approach initiates the additional and redundant render
  const votesCount = resolveVotesCount({ data, post });

  return (
    <div className="box">
      <Error errors={componentErrors}/>
      <article className="post" key={post.id}>
        <h2>
          <a href={`/posts/${post.id}`}>{post.title}</a>
        </h2>
        <div className="url">
          <a href={post.url} target="_blank">{post.url}</a>
        </div>
        <div className="tagline">{post.tagline}</div>
        <footer>
          <button className="vote-btn"
                  onClick={ () => postVoteManage({ variables: { postId: post.id } }) }>🔼 {votesCount}</button>
          <button>💬 {post.commentsCount}</button>
          <span>author: {post.user.name}</span>
        </footer>
      </article>
    </div>
  );
}

// It returns the new value from the response or an initial value from props
function resolveVotesCount({ data, post }) {
  const votesCount = data?.postVoteManage?.votesCount; // => undefined / null / number

  // NOTE(ssavitsky):
  // avoid comparing the optional response value with the initial value from props using "||"
  // because if 0 received in the response it will be ignored in favor of the initial value is 1
  return (typeof (votesCount) == 'number') ? votesCount : post.votesCount;
}

export default Post;
