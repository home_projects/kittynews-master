import * as React from 'react';
import Comment from "../Comment";

function CommentsList({ comments }) {
  if (comments.length === 0) return false;

  return (
    <>
      <div className='box'>
        <h3>Comments</h3>
        { comments.map((comment) => <Comment comment={comment} key={comment.id} />) }
      </div>
    </>
  );
}

export default CommentsList;
