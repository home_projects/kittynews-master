// This component might be used to visualize errors that come in the response when nothing was raised on a BE (validation, etc)

import * as React from 'react';

function Error(props) {
    const errors = props.errors;

    if (errors && errors.length > 0) {
        return errors.map((error, index) => <p key={index} className='error'>{error}</p>)
    } else {
        return null;
    }
}

export default Error;
