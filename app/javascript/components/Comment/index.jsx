import * as React from 'react';

function Comment({ comment: { text, user, createdAt } }) {
  return (
    <>
      <div className='box'>
        <div className="comment">
          <p>{text}</p>
          <footer>
            <span>author: {user.name}</span>
            <span> | </span>
            <span>created at: {createdAt}</span>
          </footer>
        </div>
      </div>
    </>
  );
}

export default Comment;
