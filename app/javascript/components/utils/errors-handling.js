// This encapsulates the logic useful for the "request error" handling
// "request error" means that here we expect an "error" object that comes as a standard GraphQl error in the response

// For other types of errors see:
// - to visualize validation "errors" => app/javascript/components/Error

function isAuthenticationError(error) {
  const error_code = error?.graphQLErrors?.[0]?.extensions?.code;
  return error_code === 'AUTHENTICATION_ERROR';
}

function handleError(error) {
  if (isAuthenticationError(error)) {
    // I suppose it is not reasonable to use React history and Router since it is not SPA
    return { action: () => location.href = '/users/sign_in', message: null }
  } else {
    return { action: null, message: `Error! ${error.message}` }
  }
}

export {handleError};
