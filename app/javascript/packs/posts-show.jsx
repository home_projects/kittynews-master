import * as React from 'react';
import renderComponent from './utils/renderComponent';
import gql from "graphql-tag";
import {useQuery} from "react-apollo";
import Post from "../components/Post";
import CommentsList from "../components/CommentList";

const QUERY = gql`
  query PostPage($postId: ID!) {
    post(id: $postId) {
      id
      title
      tagline
      url
      commentsCount
      votesCount
      user {
        id
        name
      }
      comments {
        id
        text
        createdAt
        user {
          id
          name
        }
      }
    }
  }
`;

function PostsShow({ postId }) {
  const { data, loading, error } = useQuery(QUERY, { variables: { postId } });

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  const { post } = data;

  return (
    <>
      <Post post={post} key={post.id} />
      <CommentsList comments={post.comments} />
    </>
  );
}

renderComponent(PostsShow);
